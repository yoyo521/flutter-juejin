import 'package:flutter/material.dart';

class HomePageEntrance extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePageEntrance> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor:Color.fromARGB(1,191,237,242) ,
      body: Column(
        children: <Widget>[
          _HeaderGener(),
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(0),
              children: <Widget>[
                _CardItemGener(),
                _CardItemGener(),
                _CardItemGener(),
                _CardItemGener(),
                _CardItemGener(),
                _CardItemGener(),
              ],
            ),
            flex: 1,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromRGBO(0, 126, 254, 1),
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

class _SearchHeader extends StatelessWidget {
  _tapGoSearch() {}
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 80,
      child: Row(
        children: <Widget>[
          GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  color: Color.fromRGBO(50, 152, 254, 1)),
              width: 280,
              padding: EdgeInsets.only(left: 10, top: 4, bottom: 2),
              margin: EdgeInsets.only(top: 28, left: 20),
              child: Row(
                children: <Widget>[
                  Icon(Icons.search, color: Colors.white),
                  Container(
                      child: Text(
                        '搜索文章,用户,标签',
                        style: TextStyle(color: Colors.white54),
                      ),
                      margin: EdgeInsets.only(left: 6, top: 0))
                ],
              ),
            ),
            onTap: _tapGoSearch,
          ),
          Container(
            margin: EdgeInsets.only(top: 25, left: 30),
            child: Row(
              children: <Widget>[
                Icon(Icons.settings, color: Colors.white),
                Text(
                  '标签',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

// 头部分的 create
class _HeaderGener extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Header();
  }
}

// 数据列表的 create
class _CardItemGener extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CardItem();
  }
}

// 头部分的具体实现
class Header extends State with SingleTickerProviderStateMixin {
  TabController tabController;
  @override
  void initState() {
    // TODO: implement inwasitState
    super.initState();
    tabController = new TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Color.fromRGBO(0, 126, 254, 1),
      child: Column(
        children: <Widget>[
          _SearchHeader(),
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Container(
                      child: TabBar(
                        indicatorColor: Colors.white,
                        controller: tabController,
                        isScrollable: false,
                        tabs: <Widget>[
                          Tab(text: '关注'),
                          Tab(text: '推荐'),
                          Tab(text: '热榜'),
                        ],
                      ),
                    ),
                    flex: 5),
                Expanded(
                    child: Container(
                        child: Icon(Icons.arrow_drop_down, color: Colors.white),
                        margin: EdgeInsets.only(left: 150)),
                    flex: 5)
              ],
            ),
          )
        ],
      ),
    );
  }
}

// 数据列表的item 列表
class CardItem extends State {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        elevation: 0,
        child: Container(
          padding: EdgeInsets.only(top: 5, bottom: 5, left: 3),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    height: 25,
                    width: 25,
                    margin: EdgeInsets.only(left: 8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        image: DecorationImage(
                            image: NetworkImage(
                                'https://user-gold-cdn.xitu.io/2018/4/26/163012e08df4aa46?imageView2/1/w/180/h/180/q/85/format/webp/interlace/1'))),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 4),
                    child: Text('咸鱼技术部',
                        style: TextStyle(color: Colors.black45, fontSize: 13)),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 195),
                    child: Text(
                      'text / javascript',
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                    ),
                  )
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(
                            top: 5,
                            left: 6,
                          ),
                          alignment: Alignment.topLeft,
                          height: 25,
                          child: Text(
                            "在flutter里面调用一个上传接口来上传文件，需要怎么做，我想用dio，接口那边是formdata？",
                            style: TextStyle(
                              fontSize: 16,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Container(
                          height: 70,
                          margin: EdgeInsets.only(left: 6, top: 2),
                          alignment: Alignment.topLeft,
                          child: Text(
                              '谢邀。 首先，要上传文件，首先得使用dio库，就跟题目中说的那样，具体怎么使用呢？先卖个关子，我们来讲讲FormData是什么。 首先，我个人不建议使用FormData来传递，因为我不会。。。 个人只会TCP 首先，服务端，Java是第一选择，这个必须承认，个人做的软件一般都是使用ServerSocket，然后客户端连接，因为',
                              style: TextStyle(color: Colors.black45,fontSize: 13),
                              softWrap: true),
                        )
                      ],
                    ),
                    flex: 7,
                  ),
                  Expanded(
                      child: Container(
                        height: 100,
                        alignment: Alignment.topLeft,
                        child: Image.network(
                            'https://pic1.zhimg.com/v2-e5cdb90fe29371c9076937e62f4e4673_xl.jpg'),
                      ),
                      flex: 3)
                ],
              )
            ],
          ),
        ),
        margin: EdgeInsets.only(left: 0, right: 0, top: 8),
      ),
    );
  }
}
