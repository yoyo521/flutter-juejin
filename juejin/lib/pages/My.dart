import 'package:flutter/material.dart';

class MyPageEntrance extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyPage();
  }
}

class _MyPage extends State<MyPageEntrance> {
  TextStyle footStyle;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    footStyle = TextStyle(color: Colors.grey, fontSize: 13);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Color.fromARGB(1, 244, 246, 249),
        body: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 25),
                    alignment: Alignment.center,
                    child: Text(
                      '我',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    height: 70,
                    color: Color.fromRGBO(0, 126, 254, 1),
                  ),
                  flex: 1,
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              color: Colors.white,
              child: ListTile(
                leading: Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                          "https://avatar.gitee.com/uploads/86/5109286_xxjbbking.png!avatar100?1563529977"),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(40.0),
                    ),
                  ),
                ),
                title: Text(
                  'singee',
                  style: TextStyle(fontSize: 20),
                ),
                trailing: Icon(Icons.keyboard_arrow_right),
                subtitle: Text('饮水机管理员', style: TextStyle(fontSize: 13)),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 0),
                child: Column(
                  children: <Widget>[
                    _MyOrpreatingItem('消息中心',
                        icon: Icon(
                          Icons.markunread,
                          color: Colors.blueAccent,
                          size: 25,
                        )),
                    _MyOrpreatingItem('我赞过的',
                        icon: Icon(
                          Icons.pan_tool,
                          color: Colors.green,
                          size: 25,
                        ),
                        foot: Text(
                          '4篇',
                          style: TextStyle(color: Colors.grey, fontSize: 13),
                        )),
                    _MyOrpreatingItem('收藏集',
                        icon: Icon(
                          Icons.bookmark,
                          color: Colors.orange,
                          size: 25,
                        ),
                        foot: Text('3个',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 13))),
                    _MyOrpreatingItem('已购小册',
                        icon: Icon(
                          Icons.card_travel,
                          color: Colors.orange,
                          size: 25,
                        ),
                        foot: Text('0本',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 13))),
                    _MyOrpreatingItem('阅读过得文章',
                        icon: Icon(
                          Icons.dehaze,
                          color: Colors.orange,
                          size: 25,
                        ),
                        foot: Text('321篇',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 13))),
                    _MyOrpreatingItem('标签管理',
                        icon: Icon(
                          Icons.tag_faces,
                          color: Colors.orange,
                          size: 25,
                        ),
                        foot: Text('7个',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 13))),
                    Container(
                        child: Column(
                          children: <Widget>[
                            _MyOrpreatingItem('夜间模式',
                                icon: Icon(
                                  Icons.wb_sunny,
                                  color: Colors.grey,
                                  size: 25,
                                ),
                              foot: Switch(value: false , onChanged: (v ) {

                              }),
                               ),
                            _MyOrpreatingItem('意见反馈',
                                icon: Icon(
                                  Icons.satellite,
                                  color: Colors.grey,
                                  size: 25,
                                ),
                               ),
                          ],
                        ),
                        margin: EdgeInsets.only(top: 10)),
                    _MyOrpreatingItem('设置',
                        icon: Icon(
                          Icons.settings,
                          color: Colors.grey,
                          size: 25,
                        ),
                       ),
                  ],
                ),
              ),
              flex: 1,
            )
          ],
        ));
  }
}

class _MyOrpreatingItem extends StatelessWidget {
  // item 标题
  String title;

  Icon icon;
  // icon 颜色
  Color leadColor = Colors.blueAccent;
  // item 背景颜色
  Color backgroundColor = Colors.white;
  // 是否需要下划线
  bool isUnderline = false;
  // 尾部
  Widget foot;

  _MyOrpreatingItem(@required this.title,
      {this.icon,
      this.leadColor,
      this.backgroundColor = Colors.white,
      this.foot}) {
    if (icon == null) {
      this.icon = Icon(
        Icons.inbox,
        color: this.leadColor != null ? this.leadColor : Colors.grey,
        size: 25,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.only(top: 1),
      decoration: BoxDecoration(
          color: backgroundColor,
          border: isUnderline
              ? Border(
                  bottom: BorderSide(
                  color: Color.fromRGBO(230, 230, 230, .8),
                  width: 1,
                ))
              : null),
      padding: EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
      child: Row(
        children: <Widget>[
          this.icon,
          Container(
              child: Text(
                title,
                style: TextStyle(),
              ),
              padding: EdgeInsets.only(left: 20)),
          Expanded(
              child: Container(
                child: foot,
                alignment: Alignment.topRight,
              ),
              flex: 1)
        ],
      ),
    );
  }
}
