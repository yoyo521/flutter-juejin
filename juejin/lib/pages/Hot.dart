import 'package:flutter/material.dart';

class HotPageEntrance extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HotPage();
  }
}

class _HotPage extends State<HotPageEntrance>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  Color baseColor = Color.fromRGBO(138, 145, 141  , 1 );

  double _footIconSize = 20;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = new TabController(length: 6, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color.fromARGB(1,191,237,242),
      body: Column(
        children: <Widget>[
          Container(
            height: 60,
            decoration: BoxDecoration(color: Color.fromRGBO(0, 126, 254, 1)),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: 30),
                    height: 50,
                    child: TabBar(
                      indicatorColor: Colors.white,
                      controller: tabController,
                      tabs: <Widget>[
                        Tab(text: '关注'),
                        Tab(text: '推荐'),
                        Tab(text: '热门'),
                        Tab(text: '开源推荐'),
                        Tab(text: '内推招聘'),
                        Tab(
                          text: '掘金相亲',
                        )
                      ],
                    ),
                  ),
                  flex: 9,
                ),
                Expanded(
                  child: Container(
                    child: Icon(
                      Icons.arrow_drop_down,
                      color: Colors.white,
                    ),
                    margin: EdgeInsets.only(top: 30),
                  ),
                  flex: 1,
                )
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: RefreshIndicator(
              onRefresh: () {},
              child: Container(
                child: ListView(
                  padding: EdgeInsets.all(0),
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      margin:EdgeInsets.only(bottom:10),
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            "https://upload.jianshu.io/users/upload_avatars/10986034/2cc9b70b-70ab-44f6-a402-5eeb1cb18d84.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/96/h/96"),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(40.0),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 6,
                                  child: Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Next',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            'Android @ 前掘金 · 一天前',
                                            style: TextStyle(
                                                color: baseColor, fontSize: 12),
                                          )
                                        ],
                                      )),
                                ),
                                Expanded(
                                    flex: 3,
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                            
                                            padding: EdgeInsets.only(
                                                left: 5,
                                                right: 5,
                                                top: 4,
                                                bottom: 3),
                                            child: Row(
                                              children: <Widget>[
                                                Icon(Icons.add,
                                                    size: 18,
                                                    color: Colors.grey),
                                                Text(
                                                  '关注',
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.grey),
                                                )
                                              ],
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(3),
                                                border: Border.fromBorderSide(
                                                    BorderSide(
                                                        width: 1,
                                                        color: Color.fromRGBO(64, 207, 96  , 1)))),
                                          ),
                                          Container(
                                            child: Icon(Icons.more_horiz,
                                                color: Colors.grey),
                                          )
                                        ],
                                      ),
                                    ))
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Text(
                                '最近公司新开了一条业务线，有幸和大佬们一起从头开始构建一套适合新业务的框架。俗话说得好呀，适合自己的才是最好的 😎。在新项目的 CodeReview 的时候，被大哥提到有没有添加 fastClick 解决移动端 300ms 延迟的问题'),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      margin:EdgeInsets.only(bottom:10),
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            "https://upload.jianshu.io/users/upload_avatars/10986034/2cc9b70b-70ab-44f6-a402-5eeb1cb18d84.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/96/h/96"),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(40.0),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 6,
                                  child: Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Next',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            'Android @ 前掘金 · 一天前',
                                            style: TextStyle(
                                                color: baseColor, fontSize: 12),
                                          )
                                        ],
                                      )),
                                ),
                                Expanded(
                                    flex: 3,
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(

                                            padding: EdgeInsets.only(
                                                left: 5,
                                                right: 5,
                                                top: 4,
                                                bottom: 3),
                                            child: Row(
                                              children: <Widget>[
                                                Icon(Icons.add,
                                                    size: 18,
                                                    color: Colors.grey),
                                                Text(
                                                  '关注',
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.grey),
                                                )
                                              ],
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(3),
                                                border: Border.fromBorderSide(
                                                    BorderSide(
                                                        width: 1,
                                                        color: Color.fromRGBO(64, 207, 96  , 1)))),
                                          ),
                                          Container(
                                            child: Icon(Icons.more_horiz,
                                                color: Colors.grey),
                                          )
                                        ],
                                      ),
                                    ))
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Text(
                                '最近公司新开了一条业务线，有幸和大佬们一起从头开始构建一套适合新业务的框架。俗话说得好呀，适合自己的才是最好的 😎。在新项目的 CodeReview 的时候，被大哥提到有没有添加 fastClick 解决移动端 300ms 延迟的问题'),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      margin:EdgeInsets.only(bottom:10),
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            "https://upload.jianshu.io/users/upload_avatars/10986034/2cc9b70b-70ab-44f6-a402-5eeb1cb18d84.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/96/h/96"),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(40.0),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 6,
                                  child: Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Next',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            'Android @ 前掘金 · 一天前',
                                            style: TextStyle(
                                                color: baseColor, fontSize: 12),
                                          )
                                        ],
                                      )),
                                ),
                                Expanded(
                                    flex: 3,
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(

                                            padding: EdgeInsets.only(
                                                left: 5,
                                                right: 5,
                                                top: 4,
                                                bottom: 3),
                                            child: Row(
                                              children: <Widget>[
                                                Icon(Icons.add,
                                                    size: 18,
                                                    color: Colors.grey),
                                                Text(
                                                  '关注',
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.grey),
                                                )
                                              ],
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(3),
                                                border: Border.fromBorderSide(
                                                    BorderSide(
                                                        width: 1,
                                                        color: Color.fromRGBO(64, 207, 96  , 1)))),
                                          ),
                                          Container(
                                            child: Icon(Icons.more_horiz,
                                                color: Colors.grey),
                                          )
                                        ],
                                      ),
                                    ))
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Text(
                                '最近公司新开了一条业务线，有幸和大佬们一起从头开始构建一套适合新业务的框架。俗话说得好呀，适合自己的才是最好的 😎。在新项目的 CodeReview 的时候，被大哥提到有没有添加 fastClick 解决移动端 300ms 延迟的问题'),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      margin:EdgeInsets.only(bottom:10),
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            "https://upload.jianshu.io/users/upload_avatars/10986034/2cc9b70b-70ab-44f6-a402-5eeb1cb18d84.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/96/h/96"),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(40.0),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 6,
                                  child: Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Next',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            'Android @ 前掘金 · 一天前',
                                            style: TextStyle(
                                                color: baseColor, fontSize: 12),
                                          )
                                        ],
                                      )),
                                ),
                                Expanded(
                                    flex: 3,
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(

                                            padding: EdgeInsets.only(
                                                left: 5,
                                                right: 5,
                                                top: 4,
                                                bottom: 3),
                                            child: Row(
                                              children: <Widget>[
                                                Icon(Icons.add,
                                                    size: 18,
                                                    color: Colors.grey),
                                                Text(
                                                  '关注',
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.grey),
                                                )
                                              ],
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(3),
                                                border: Border.fromBorderSide(
                                                    BorderSide(
                                                        width: 1,
                                                        color: Color.fromRGBO(64, 207, 96  , 1)))),
                                          ),
                                          Container(
                                            child: Icon(Icons.more_horiz,
                                                color: Colors.grey),
                                          )
                                        ],
                                      ),
                                    ))
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Text(
                                '最近公司新开了一条业务线，有幸和大佬们一起从头开始构建一套适合新业务的框架。俗话说得好呀，适合自己的才是最好的 😎。在新项目的 CodeReview 的时候，被大哥提到有没有添加 fastClick 解决移动端 300ms 延迟的问题'),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                                Container(child: Container(child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.markunread,
                                      color: baseColor,
                                      size: _footIconSize,
                                    ),
                                    Text(
                                      '9',
                                      style: TextStyle(color: baseColor),
                                    )
                                  ],
                                ),),),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
