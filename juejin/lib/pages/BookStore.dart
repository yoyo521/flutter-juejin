import 'package:flutter/material.dart';

class BookStorePageEntrance extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BookStorePage();
  }
}

class _BookStorePage extends State<BookStorePageEntrance>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Container(
            color: Color.fromRGBO(0, 126, 254, 1),
            child: Center(
              child: Container(
                height: 70,
                width: 150,
                padding: EdgeInsets.only(top: 30),
                child: TabBar(
                    controller: TabController(length: 2, vsync: this),
                    tabs: [
                      Tab(
                        child: Text(
                          '全部',
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Tab(
                        child: Text('已购', style: TextStyle(fontSize: 16)),
                      ),
                    ]),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child:_GenBookItemList() ,
            ),
          )
        ],
      ),
    );
  }
}



class _GenBookItemList extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BookItemList() ;
  }
}
class _BookItemList extends State<_GenBookItemList> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      padding: EdgeInsets.all(0),
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ), 
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top:5),
          padding: EdgeInsets.only(left:3),
          decoration: BoxDecoration(color:Colors.white,border: Border(bottom: BorderSide(width: 1, color:Color.fromRGBO(231, 231, 231, .7)))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                    padding: EdgeInsets.all(5),
                    height: 110,
                    child: Image(
                      width: 25,
                      image: NetworkImage(
                          'https://user-gold-cdn.xitu.io/2019/6/3/16b1977c2f3a0ed0?w=1950&h=2730&f=png&s=545408'),
                    )),
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(margin:EdgeInsets.only(top:8) ,child:  Text('[视频] 图解 Kafka 之核心原理之核心原理之核心原理',
                        style: TextStyle(fontSize: 16)),),
                    Container(margin:EdgeInsets.only(top:10) , child: Text(
                      '技术胖',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left,
                    ),),
                    Container( margin:EdgeInsets.only(top:10) ,child: Text(
                      '70小节 · 279人购买',
                      style:
                      TextStyle(color: Colors.grey, fontSize: 12),
                    ),)
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(top:40),
                  child: Center(child: Text('￥19.9' , style: TextStyle(color: Color.fromRGBO(0, 126, 254, 1)),),),
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Color.fromRGBO(241, 247, 244, 1)),
                ),
              )
            ],
          ),
        ),
      ],
    ) ;
  }
}