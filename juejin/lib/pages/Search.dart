import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class SearchPageEntrance extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SearchPage();
  }
}

class _SearchPage extends State<SearchPageEntrance> {
  List swiperImages = [
    'https://th.wallhaven.cc/small/6k/6kw5z6.jpg',
    'https://th.wallhaven.cc/lg/j5/j5y525.jpg'
  ];

  // 轮播图下面的 icon 大小
  double iconSize = 37.0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Color.fromARGB(1, 191, 237, 242),
        body: Column(
          children: <Widget>[
            Container(
              height: 70,
              padding: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(color: Color.fromRGBO(0, 126, 254, 1)),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2.0),
                            color: Color.fromRGBO(50, 152, 254, 1)),
                        padding: EdgeInsets.only(left: 5, top: 2, bottom: 2),
                        margin: EdgeInsets.only(top: 25, left: 10),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.search, color: Colors.white),
                            Container(
                                child: Text(
                                  '搜索文章,用户,标签',
                                  style: TextStyle(color: Colors.white54),
                                ),
                                margin: EdgeInsets.only(left: 6, top: 0))
                          ],
                        ),
                      ),
                      onTap: () {},
                    ),
                  ),
                ],
              ),
            ),
            Container(
                child: Swiper(
                  itemCount: swiperImages.length,
                  autoplay: true,
                  itemBuilder: (context, i) {
                    return Image.network(
                      swiperImages[i],
                      fit: BoxFit.cover,
                    );
                  },
                ),
                height: 130),
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Image.asset(
                            'assets/icons/search-icon1.png',
                            width: iconSize,
                          ),
                          Text('文章榜')
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Image.asset(
                            'assets/icons/search-icon2.png',
                            width: iconSize,
                          ),
                          Text('作者榜')
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Image.asset(
                            'assets/icons/search-icon3.png',
                            width: iconSize,
                          ),
                          Text('看一看')
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Image.asset(
                            'assets/icons/search-icon4.png',
                            width: iconSize,
                          ),
                          Text('话题广场')
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Image.asset(
                            'assets/icons/search-icon5.png',
                            width: iconSize,
                          ),
                          Text('活动')
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            GenHotArticle()
          ],
        ));
  }
}

class GenHotArticle extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HotArticle();
  }
}

class HotArticle extends State<GenHotArticle> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Expanded(
      flex: 1,
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.only(top: 10),
        child: ListView(
          padding: EdgeInsets.only(top: 0),
          children: <Widget>[
            ListTile(
              title: Row(
                children: <Widget>[
                  Container(
                      child: Icon(Icons.dehaze, color: Colors.red),
                      margin: EdgeInsets.only(top: 3)),
                  Text('热门文章')
                ],
              ),
              trailing: Text('定制热门'),
            ),
            Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),  Container(
              height: 100,
              padding: EdgeInsets.only(top:10,left:5,right:5),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1, color: Color.fromRGBO(175, 175, 175, .2)))),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                '三刷红宝书之 JavaScript 的引用类型',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 5),
                              child: Text(
                                '6人赞·yeyan1997·四小时前',
                                style: TextStyle(
                                    color: Color.fromRGBO(175, 175, 175, .7),
                                    fontSize: 13),
                              ),
                            )
                          ],
                        )),
                    flex: 8,
                  ),
                  Expanded(
                      child: Container( alignment: Alignment.topLeft,child: Container(
                        height: 80,
                        color: Colors.red,
                        child: Image.network(
                            'https://ss3.baidu.com/-rVXeDTa2gU2pMbgoY3K/it/u=2922130382,676357541&fm=202&mola=new&crop=v1',
                            fit: BoxFit.cover),
                      )),
                      flex: 2)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
