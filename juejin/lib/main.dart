

import 'package:flutter/material.dart';
import 'package:juejin/pages/Home.dart';
import 'package:juejin/pages/Hot.dart';
import 'package:juejin/pages/Search.dart';
import 'package:juejin/pages/BookStore.dart';
import 'package:juejin/pages/My.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final Color primaryColor = Colors.blue;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '掘金',
      theme: ThemeData(
        primarySwatch: primaryColor,
      ),
      home: MyHomePage(title: 'juejin.im'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<Widget> pages =[
    HomePageEntrance(),
    HotPageEntrance(),
    SearchPageEntrance(),
    BookStorePageEntrance(),
    MyPageEntrance()
  ];

  int _bottomIndex = 0 ;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: pages[_bottomIndex],
      bottomNavigationBar: new BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.home,
                size: 28,
              ),
              color: _bottomIndex == 0 ? Colors.blue : Colors.grey,
              onPressed: () => setState(() => _bottomIndex = 0),
            ),
            IconButton(
              icon: Icon(Icons.supervised_user_circle, size: 28),
              color: _bottomIndex == 1 ? Colors.blue : Colors.grey,
              onPressed: () => setState(() => _bottomIndex = 1),
            ),
            IconButton(
              icon: Icon(
                Icons.search,
                size: 28,
              ),
              color: _bottomIndex == 2 ? Colors.blue : Colors.grey,
              onPressed: () => setState(() => _bottomIndex = 2),
            ),
            IconButton(
              icon: Icon(
                Icons.book,
                size: 28,
              ),
              color: _bottomIndex == 3 ? Colors.blue : Colors.grey,
              onPressed: () => setState(() => _bottomIndex = 3),
            ),
            IconButton(
              icon: Icon(
                Icons.person,
                size: 28,
              ),
              color: _bottomIndex == 4 ? Colors.blue : Colors.grey,
              onPressed: () => setState(() => _bottomIndex = 4),
            ),
          ],
        ),
      ), //bottomAppBar
      // This trailing comma makes auto-formatting nicer for build methods.
    ); ;
  }
}
