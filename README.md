# flutter-juejin 采用flutter 编写掘金手机APP

## 环境

```


    Flutter 1.7.8+hotfix.4 • channel stable • https://github.com/flutter/flutter.git
    
    Framework • revision 20e59316b8 (3 weeks ago) • 2019-07-18 20:04:33 -0700
    
    Engine • revision fee001c93f
    
    Tools • Dart 2.4.0


```

## 页面截图

####  ![首页](./首页预览.png)

![我的](./我的页面预览.png)

![](./沸点预览.png)

![书城](./书城预览.png)

![搜索](./搜索页面预览.png)

